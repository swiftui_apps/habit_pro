//
//  HabitProTests.swift
//  HabitProTests
//
//  Created by Imthath M on 10/02/20.
//  Copyright © 2020 SkyDevz. All rights reserved.
//

import XCTest
@testable import HabitPro

class HabitProTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testWeekdaysConversion() {
        for _ in 0...5 {
            let habit = Model.habit
            print("active days numbers - \(habit.activeDays)")
            print("days:")
            for day in habit.activeOn {
                print(day.name())
            }
        }
        
    }

}
