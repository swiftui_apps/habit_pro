//
//  SettingsView.swift
//  HabitPro
//
//  Created by Imthath M on 12/02/20.
//  Copyright © 2020 SkyDevz. All rights reserved.
//

import SwiftUI

struct SettingsView: View {
    
    @Binding var isNavBarHidden: Bool
    
    var body: some View {
        Text("Settings")
        .navigationBarTitle("Settings")
        .navigationBarHidden(isNavBarHidden)
            .onAppear{ self.isNavBarHidden = false }
    }
}
