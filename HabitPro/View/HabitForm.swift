//
//  HabitForm.swift
//  HabitPro
//
//  Created by Imthath M on 12/02/20.
//  Copyright © 2020 SkyDevz. All rights reserved.
//

import SwiftUI

struct HabitForm: View {
    var body: some View {
        Text("Add a habit here")
    }
}

struct HabitForm_Previews: PreviewProvider {
    static var previews: some View {
        HabitForm()
    }
}
