//
//  HabitList.swift
//  HabitPro
//
//  Created by Imthath M on 10/02/20.
//  Copyright © 2020 SkyDevz. All rights reserved.
//

import SwiftUI
import SwiftDate
import SDComponents

var TODAY: Int { 5 }

struct HabitList: View {
    
    @State var habits: [CDHabit]
    @State var isNavBarHidden: Bool = true
    @State var showHabitFrom: Bool = false
    
    var body: some View {
        NavigationView {
            viewStack
                .navigationBarTitle("")
                .navigationBarHidden(isNavBarHidden)
                .onAppear { self.isNavBarHidden = true }
                .popover(isPresented: $showHabitFrom, content: { HabitForm() })
        }
    }
    
    var viewStack: some View {
        ZStack(alignment: .bottom) {
            verticalStack
            Button(action: { self.showHabitFrom = true }, label: {
                SDImage.plus.font(.largeTitle)
            })
        }
    }
    
    var verticalStack: some View {
        VStack(spacing: .zero) {
            customBar
            Divider()
            dayStack
                .frame(height: .averageTouchSize)
                .padding(.leading, .large*2)
            Divider()
            listView
            EmptyView().frame(height: .averageTouchSize)
        }
    }
    
    var customBar: some View {
        HStack {
            Spacer()
            NavigationLink(destination: SettingsView(isNavBarHidden: $isNavBarHidden)) {
                Image(systemName: "gear")
                    .font(.title)
//                    .foregroundColor(.blue)
//                .padding()
            }
            
        }.padding()
    }
    
    var dayStack: some View {
        GeometryReader { geometry in
            HStack {
                ForEach(Constants.weekDays, id: \.self) { day in
                    Text(day.name().prefix(1))
                        .frame(width: geometry.size.width / 8.5)
                        .background((day.rawValue == TODAY) ? Color.blue : Color.clear)
                        .clipShape(Circle())
                }
            }.frame(width: geometry.size.width)
        }
    }
    
    var listView: some View {
        List {
            ForEach(habits) { habit in
                HabitRow(habit: habit)
                    .listRowInsets(EdgeInsets())
            }
        }
        
    }
}

extension CDHabit: Identifiable { }

struct WeekDayCircle: View {
    
    var day: WeekDay
    var habit: CDHabit
    
    var body: some View {
        VStack(spacing: .zero) {
            if Bool.random() {
                completedInactive
            }
            
            inactive
        }
        
    }
    
    
    var inactive: some View {
        thinCirlce
            .foregroundColor(.gray)
    }
    
    var completedInactive: some View {
        SDImage.roundedCheck
            .foregroundColor(.gray)
    }
    
    var thinCirlce: some View {
        Image(systemName: "circle")
            .font(.system(size: .large, weight: .thin, design: .default))
    }
}

struct HabitList_Previews: PreviewProvider {
    static var previews: some View {
        HabitList(habits: Model.habits)
    }
}
