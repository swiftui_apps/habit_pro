//
//  HabitRow.swift
//  HabitPro
//
//  Created by Imthath M on 12/02/20.
//  Copyright © 2020 SkyDevz. All rights reserved.
//

import SwiftUI
import SwiftDate
import SDComponents

struct HabitRow: View {
    
    let habit: CDHabit
    
    let progess = Model.progress(for: Model.habit, from: Date())
    
    var body: some View {
        VStack(alignment: .leading, spacing: .small) {
            Text(habit.name).padding(EdgeInsets(top: .small, leading: .medium, bottom: .small, trailing: .small))
            checkmarks.padding(EdgeInsets(top: .zero, leading: .large*2, bottom: .medium, trailing: .small))
        }
    }
    
    var checkmarks: some View {
        GeometryReader { geometry in
            HStack {
                ForEach(0...6, id: \.self) { index in
                    HabitButton(habit: self.habit, day: Constants.weekDays[index], progress: self.progess[index])
                        .frame(width: geometry.size.width / 8.5)
                    
                }
                //                ForEach(Constants.weekDays, id: \.self) { day in
                //                    self.getImage(in: geometry.size, for: day)
                //                        .frame(width: geometry.size.width / 8.5)
                //                }
            }.frame(width: geometry.size.width).padding()
        }
    }
}

struct HabitButton: View {
    
    let habit: CDHabit
    let day: WeekDay
    var progress: CDProgress
    
    var body: some View {
        Button(action: changeState, label: image)
    }
    
    func changeState() {
        switch progress.state {
        case .missed:// this will not be required, missed implies, progress is not stored
            progress.state = .done
        case .done:
            progress.state = .failed
        case .failed:
            progress.state = .done
        }
    }
    
    func image() -> some View {
        VStack(spacing: .zero) {
            if day.rawValue < TODAY  { // past
                if day.rawValue.isSimilar(to: Int(habit.activeDays)) { // active
                    if progress.state == .done {
                        completedActive
                    } else if progress.state == .failed {
                        failed
                    } else {
                        missed
                    }
                } else {
                    if progress.state == .done {
                        completedInactive
                    } else {
                        inactive
                    }
                }
                //                getPastImage(for: day, with: progress)
            } else {
                if day.rawValue.isSimilar(to: Int(habit.activeDays)) {
                    futureActive
                } else {
                    futureInactive
                }
                //                getFutureImage(for: day, with: progress)
            }
            
        }
        
    }
    
    //    func getPastImage(for day: WeekDay, with progress: CDProgress) -> some View {
    //        VStack(spacing: .zero) {
    //            if day.rawValue.isSimilar(to: Int(habit.activeDays)) { // active
    //                if progress.state == .done {
    //                    completedActive
    //                } else if progress.state == .failed {
    //                    failed
    //                } else {
    //                    missed
    //                }
    //            } else { // inactive
    //                if progress.state == .done {
    //                    completedInactive
    //                } else {
    //                    inactive
    //                }
    //            }
    //        }
    //    }
    //
    //    func getFutureImage(for day: WeekDay, with progress: CDProgress) -> some View {
    //        VStack(spacing: .zero) {
    //            if day.rawValue.isSimilar(to: Int(habit.activeDays)) {
    //                futureActive
    //            } else {
    //                futureInactive
    //            }
    //        }
    //    }
    
    var inactive: some View {
        thinCirlce
            .font(.system(size: .large))
            .foregroundColor(.gray)
    }
    
    var completedInactive: some View {
        SDImage.roundedCheck
            .font(.system(size: .large))
            .foregroundColor(.gray)
    }
    
    var completedActive: some View {
        SDImage.roundedCheck
            .font(.system(size: .large))
            .foregroundColor(.green)
    }
    
    var futureActive: some View {
        thinCirlce
            .foregroundColor(.yellow)
    }
    
    var futureInactive: some View {
        thinCirlce
            .foregroundColor(.gray)
    }
    
    var failed: some View {
        Image(systemName: "x.circle.fill")
            .font(.system(size: .large))
            .foregroundColor(.red)
    }
    
    var missed: some View {
        Image(systemName: "minus.circle.fill")
            .font(.system(size: .large))
            .foregroundColor(.orange)
    }
    
    var thinCirlce: some View {
        Image(systemName: "circle")
            .font(.system(size: .large, weight: .thin, design: .default))
    }
}

struct HabitRow_Previews: PreviewProvider {
    static var previews: some View {
        HabitRow(habit: Model.habit)
    }
}
