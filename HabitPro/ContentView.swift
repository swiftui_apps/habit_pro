//
//  ContentView.swift
//  HabitPro
//
//  Created by Imthath M on 10/02/20.
//  Copyright © 2020 SkyDevz. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        HabitList(habits: Model.habits)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
