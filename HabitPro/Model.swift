//
//  CDHabit.swift
//  HabitPro
//
//  Created by Imthath M on 10/02/20.
//  Copyright © 2020 SkyDevz. All rights reserved.
//

import Foundation
import CoreData

@objc(CDHabit)
class CDHabit: NSManagedObject {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDHabit> {
        return NSFetchRequest<CDHabit>(entityName: "CDHabit")
    }

    @NSManaged public var id: UUID
    @NSManaged public var name: String
    @NSManaged public var desc: String
    @NSManaged public var startDate: Date
    @NSManaged public var endDate: Date
    @NSManaged public var activeDays: Int32
    @NSManaged public var style: Style
    
    @objc enum Style: Int {
        case start, stop
    }
}

@objc(CDProgress)
class CDProgress: NSManagedObject {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDProgress> {
        return NSFetchRequest<CDProgress>(entityName: "CDProgress")
    }
    
    @NSManaged public var habitID: UUID
    @NSManaged public var date: Date
    @NSManaged public var state: State
    
    @objc enum State: Int16 {
        case done, failed, missed
    }
}

class Model {
    static var habit: CDHabit {
        let habit = CDHabit(context: Cache.coreData.context)
        habit.id = UUID()
        habit.name = ["running", "reading", "cofffe"].randomElement()!
        habit.desc = ["every morning", "control yourself", "express yourself"].randomElement()!
        habit.startDate = Date().addingTimeInterval(-100000000)
        habit.endDate = Date().addingTimeInterval(100000000)
        habit.activeDays = Int32([1357, 127, 246, 1456, 23456, 45, 67].randomElement()!)
//        habit.style = [CDHabit.Style.start, .stop].randomElement()!
        return habit
    }
    
    static var habits: [CDHabit] {
        var result = [CDHabit]()
        for _ in 0...20 {
            result.append(habit)
        }
        return result
    }
    
    static func progress(for habit: CDHabit, from date: Date) -> [CDProgress] {
        var result = [CDProgress]()
        var startDate = date
        for _ in 1...7  {
//            let progress = CDProgress(entity: CDProgress.entity(), insertInto: Cache.coreData.context)
            let progress = CDProgress(context: Cache.coreData.context)
            progress.habitID = habit.id
            progress.date = startDate
            progress.state = [CDProgress.State.done, .failed, .missed].randomElement()!
            startDate = startDate.dateByAdding(1, .day).date
            result.append(progress)
        }
        return result
    }
}
