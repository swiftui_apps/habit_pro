//
//  Constants.swift
//  HabitPro
//
//  Created by Imthath M on 11/02/20.
//  Copyright © 2020 SkyDevz. All rights reserved.
//

import SDComponents
import SwiftDate

//enum Day: Int {
//    case sunday = 1, monday, tuesday
//}
class Constants {
    static var days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
//    static var days = ["S", "M", "T", "W", "T", "F", "S"]
    static var weekDays: [WeekDay] = [.sunday, .monday, .tuesday, .wednesday, .thursday, .friday, .saturday]
}

class Cache {
    static let coreData = SDCoreData.init(modelName: "HabitPro")
    static let local = CoreDataService()
    
}

extension CDHabit {
    public var activeOn: [WeekDay] {
        var result = [WeekDay]()
        var temp = activeDays
        var remainder = temp % 10
        while remainder != 0 {
            temp /= 10
            result.appendOptional(WeekDay(rawValue: Int(remainder)))
            remainder = temp % 10
        }
        return result
    }
}

extension Array {
    mutating func appendOptional(_ optionalElement: Element?) {
        if let element = optionalElement {
            self.append(element)
        }
    }
}
